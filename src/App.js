import React, { Component } from 'react';
import Clock  from './clock/clock'
import SelectTimezone from './select-timezone/select-timezone'
import './App.css';

class App extends Component {
  render() {
    return (
        <div>
            <SelectTimezone></SelectTimezone>
            <Clock></Clock>
        </div>
    );
  }
}
export default App;
