import React, { Component } from 'react';
import selectedStore from '../shared/selectedTimezoneStore';
import moment from 'moment';
import './clock.css';

import ClockTemplate from './clockLayout';

export default class Clock extends Component {
    constructor(props){
        super(props);
        this.state = {
            date: moment(),
            timezones: []
        }
    }
    componentDidMount() {
        this.start();
        selectedStore.subscribe(() => {
            this.setState({timezones: selectedStore.getState()});
        })
    }
    start() {
        var self = this;
        (function tick() {
            self.setState({ date: moment() });
            requestAnimationFrame(tick);
        })();
    }
    render() {
        return (
            <div className="container">
                <div className="row">
                    {this.state.timezones.map((zone, i) => {
                        return <ClockTemplate key={i} timezone={zone} date={this.state.date}></ClockTemplate>
                    })}
                </div>
            </div>
        );
    }
}
