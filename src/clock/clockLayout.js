import React, { Component } from 'react';
import classNames from 'classnames';
import moment from 'moment';
import tz from 'moment-timezone';

export default class ClockTemplate extends Component{

    clocksBackground(timezone) {
        let value = timezone.split('/').slice(-1)[0].toLowerCase();
        return classNames( 'circle',  `${value}`);

    }
    
    timePreparation(date, timezone) {
        let d = date.tz(timezone);
        let millis = d.millisecond();
        let second = d.second() * 6 + millis * (6 / 1000);
        let minute = d.minute() * 6 + second / 60;
        let hour = ((d.hour() % 12) / 12) * 360 + 90 + minute / 12;
        return {
            digitalValue : d.format('LTS'),
            second: second,
            minute: minute,
            hour: hour
        };

        
    }

    render() {
        let time = this.timePreparation(this.props.date, this.props.timezone);
        return (
            <div className="col-md-3">
                <div className="clock-text">{this.props.timezone}</div>
                <div className={this.clocksBackground(this.props.timezone)}>
                    <div className="face">
                        <div className="second" style={this.transform(this.rotate(time.second))}></div>
                        <div className="hour" style={this.transform(this.rotate(time.hour))}></div>
                        <div className="minute" style={this.transform(this.rotate(time.minute))}></div>
                    </div>
                </div>
                <div className="clock-text">
                    {time.digitalValue}
                </div>
            </div>

        );
    }
    transform(str) {
        return { transform: str };
    }
    rotate(deg) {
        return 'rotate(' + deg + 'deg)';
    }

}
