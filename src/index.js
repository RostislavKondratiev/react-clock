import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore } from 'redux'

import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';


const initialState = ['Antarctica/Troll','America/Chicago', 'Asia/Hong_Kong', 'Africa/Addis_Ababa', 'Atlantic/Canary', 'Brazil/East', 'CET', 'Europe/London','Japan'];

function clocksTimezones(state = initialState, action) {
    if(action.type === "CLOCK_TIMEZONE") {
        return [
            ...state,
            action.timezone
        ];
    }
    return state;
}

const timezoneStore = createStore(clocksTimezones);

ReactDOM.render(
    <Provider store={timezoneStore}>
        <App />
    </Provider>,
    document.getElementById('root'));
registerServiceWorker();
