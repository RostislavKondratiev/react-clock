import React, { Component } from 'react';
import { connect } from 'react-redux';
import TimezoneTemplate from './select-timezoneLayout'
import selectedStore from '../shared/selectedTimezoneStore';


class SelectTimezone extends Component {

    onChangeHandler(event) {
        if(event.target.value.length > 0) {
            selectedStore.dispatch({type: 'SELECTED_TIMEZONE', value: event.target.value});
        }
    }
    
    render() {
        const optionsState = this.props.testStore;
        return(
            <div>
                <TimezoneTemplate changeHandler={this.onChangeHandler.bind(this)} list={optionsState}></TimezoneTemplate>
            </div>
        )
    }
}

export default connect(
    state => ({
        testStore: state
    }),
    dispatch => ({})
)(SelectTimezone);
