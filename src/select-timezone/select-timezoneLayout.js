import React, { Component } from 'react';

export default class SelectTimezoneTemplate extends Component {
    constructor(props) {
        super(props);
        this.state = {
            handler: props.changeHandler,
            list: props.list,
            };
    }
    
    render() {
        return (
            <select onChange={this.state.handler}>
                <option value="" default>Select a timezone</option>
                {this.state.list.map((zone, i) =>
                    <option key={i} value={zone}>{zone}</option>
                )}
            </select>
        )
    }
}