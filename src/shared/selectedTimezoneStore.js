import { createStore } from 'redux';

const selectedTimezoneReducer = function (state = [], action) {
    if(action.type === "SELECTED_TIMEZONE") {
        return state.concat([action.value]);
    }
    return state;
};

export default createStore(selectedTimezoneReducer);